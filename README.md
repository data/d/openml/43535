# OpenML dataset: The-Big-Five-European-soccer-leagues-data

https://www.openml.org/d/43535

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context

5 countries (Tha major five soccer leagues).
44269 games.
25 seasons.
226 teams.

Content
All game scores of the big five European soccer leagues (England, Germany, Spain, Italy and France) for the 1995/96 to 2019/20 seasons.
Acknowledgements
The construction of the dataset was made possible thanks to football.db
What's football.db?
  A free open public domain football database  scheme for use in any (programming) language e.g. uses datasets in (structured) text
  using the football.txt format.
  More [football.db Project Site ](http://openfootball.github.io)

Inspiration
This data set could help:
         +   Analyse the evolution of football in the 5 major leagues over the last 25 years.
         +   Prepare all kinds of dashboards on the games, seasons, teamsetc.
         +   Analyze the differences between countries in terms of league level.
         +   Identify patterns, schemes in the dataetc.
Have fun!

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43535) of an [OpenML dataset](https://www.openml.org/d/43535). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43535/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43535/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43535/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

